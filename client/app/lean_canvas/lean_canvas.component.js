'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './lean_canvas.routes';

export class LeanCanvasComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'Hello';
  }
}

export default angular.module('projectinternApp.lean_canvas', [uiRouter])
  .config(routes)
  .component('leanCanvas', {
    template: require('./lean_canvas.html'),
    controller: LeanCanvasComponent,
    controllerAs: 'leanCanvasCtrl'
  })
  .name;
