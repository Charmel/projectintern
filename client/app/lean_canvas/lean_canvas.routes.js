'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('lean_canvas', {
      url: '/lean_canvas',
      template: '<lean-canvas></lean-canvas>'
    });
}
