'use strict';

describe('Component: LeanCanvasComponent', function() {
  // load the controller's module
  beforeEach(module('projectinternApp.lean_canvas'));

  var LeanCanvasComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    LeanCanvasComponent = $componentController('lean_canvas', {});
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
